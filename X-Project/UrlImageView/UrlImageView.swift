//
//  UrlImageView.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 6/4/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import UIKit

class UrlImageView: UIImageView {

    static var imageCache = NSCache<NSURL, UIImage>()
    
    private var currentUrl: URL?
    
    func setImage(with url: URL) {
        currentUrl = url
        
        if let image = UrlImageView.imageCache.object(forKey: url as NSURL) {
            self.image = image
        } else {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let task = session.dataTask(with: url) { [weak self] (data, response, error) in
                guard let self = self else { return }
                
                if let error = error {
                    print("Failed to load image with error: \(error)")
                    return
                }
                
                if let imageData = data, let image = UIImage(data: imageData) {
                    UrlImageView.imageCache.setObject(image, forKey: url as NSURL)
                    
                    if url == self.currentUrl {
                        DispatchQueue.main.async {
                            self.image = image
                        }
                    }
                }
            }
            task.resume()
        }
    }

}
