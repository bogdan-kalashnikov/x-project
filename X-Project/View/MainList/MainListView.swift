//
//  MainListView.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 5/31/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import Foundation

struct PostDisplayModel {
    
    var title: String
    var author: String
    var date: String
    var comments: String
    var score: String
    var thumbnail: URL?
    var isSelectable: Bool
    
}

protocol MainListView: AnyObject {
    
    func displayPosts(_ posts: [PostDisplayModel])
    
    func presentSafariView(with url: URL)
    
    func beginUpdating()
    func endUpdating()
    
}
