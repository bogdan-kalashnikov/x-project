//
//  MainListTableViewCell.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 5/31/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import UIKit

class MainListTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var authorLabel: UILabel!
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var commentsLabel: UILabel!
    @IBOutlet private var scoreLabel: UILabel!
    
    @IBOutlet private var thumbnailImageView: UrlImageView!
    
    // MARK: - UITableViewCell
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = ""
        authorLabel.text = ""
        dateLabel.text = ""
        commentsLabel.text = ""
        scoreLabel.text = ""
        thumbnailImageView.image = #imageLiteral(resourceName: "placeholder")
    }

    // MARK: - Configuration
    
    func configure(post: PostDisplayModel) {
        titleLabel.text = post.title
        authorLabel.text = post.author
        dateLabel.text = post.date
        commentsLabel.text = post.comments
        scoreLabel.text = post.score
        accessoryType = (post.isSelectable ? .disclosureIndicator : .none)
        
        if let thumbnailUrl = post.thumbnail {
            thumbnailImageView.setImage(with: thumbnailUrl)
        }
    }

}
