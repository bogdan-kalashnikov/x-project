//
//  MainListViewController.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 5/31/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import UIKit
import SafariServices

class MainListViewController: UIViewController {
    
    private let mainCellReuseId = "\(MainListTableViewCell.self)"
    private let loadingCellReuseId = "\(LoadingTableViewCell.self)"
    
    // MARK: - Properties
    
    private var mainListPresenter: MainListPresenterProtocol = MainListPresenter(dataSource: CoreDataManager.shared)
    private var refreshControl = UIRefreshControl()
    
    private var posts: [PostDisplayModel] = [] {
        didSet {
            updateView()
        }
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet private var tableView: UITableView!

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        mainListPresenter.attachView(self)
        loadData(nil)
    }
    
    // MARK: - Configuration
    
    private func configureView() {
        refreshControl.addTarget(self, action: #selector(loadData(_:)), for: .valueChanged)
        
        tableView.refreshControl = refreshControl
        tableView.tableFooterView = UIView()
    }
    
    // MARK: - Update
    
    @objc
    private func loadData(_ sender: AnyObject?) {
        mainListPresenter.loadData(option: (sender is UIRefreshControl) ? .reload : .all)
    }
    
    private func updateView() {
        tableView.reloadData()
    }

}

// MARK: - MainListView

extension MainListViewController: MainListView {
    
    func displayPosts(_ posts: [PostDisplayModel]) {
        self.posts = posts
    }
    
    func presentSafariView(with url: URL) {
        let controller = SFSafariViewController(url: url)
        controller.delegate = self
        present(controller, animated: true)
    }
    
    func beginUpdating() {
        refreshControl.beginRefreshing()
    }
    
    func endUpdating() {
        refreshControl.endRefreshing()
    }
    
}

// MARK: - UITableViewDataSource

extension MainListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.isEmpty ? 0 : (posts.count + 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = (indexPath.row == posts.count) ? loadingCellReuseId : mainCellReuseId
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        if let mainCell = cell as? MainListTableViewCell {
            mainCell.configure(post: posts[indexPath.row])
        } else if let loadingCell = cell as? LoadingTableViewCell {
            loadingCell.isAnimating = true
        }
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension MainListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == posts.count else { return }
        mainListPresenter.loadData(option: .next)
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        guard indexPath.row < posts.count else { return false }
        return posts[indexPath.row].isSelectable
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        mainListPresenter.didSelectPost(at: indexPath.row)
    }
    
}

// MARK: - SFSafariViewControllerDelegate

extension MainListViewController: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true)
    }
    
}
