//
//  LoadingTableViewCell.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 6/6/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    
    var isAnimating: Bool = false {
        didSet {
            isAnimating ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
        }
    }

    // MARK: IBOutlets
    
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!

}
