//
//  UserDefaults+After.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 6/6/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    fileprivate enum CodingKeys {
        static let after = "AfterUserDefaultsKey"
    }
    
    static var after: String? {
        get {
            return UserDefaults.standard.string(forKey: CodingKeys.after)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: CodingKeys.after)
        }
    }
}
