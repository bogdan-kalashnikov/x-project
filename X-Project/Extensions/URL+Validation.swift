//
//  URL+Validation.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 6/5/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import UIKit

extension URL {
    
    var isValid: Bool {
        return UIApplication.shared.canOpenURL(self)
    }
    
}
