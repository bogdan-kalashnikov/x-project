//
//  CDPost+Init.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 6/5/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import CoreData

extension CDPost {
    
    convenience init(post: Post, context: NSManagedObjectContext) {
        self.init(context: context)
        title = post.title
        author = post.author
        createdDate = post.createdDate
        numberOfComments = Int64(post.numberOfComments)
        score = Int64(post.score)
        
        url = post.url
        thumbnailUrl = post.thumbnailUrl
    }
    
}
