//
//  PostDataSource.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 6/5/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import Foundation

protocol PostDataSource: AnyObject {
    
    func getAll() -> [Post]
    func save(_ posts: [Post])
    func save(_ post: Post)
    func deleteAll()
    
}
