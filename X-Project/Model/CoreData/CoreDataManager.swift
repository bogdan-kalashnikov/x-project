//
//  CoreDataManager.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 6/5/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataManager: NSObject {
    
    static let shared = CoreDataManager()
    
    private let modelName = "XProjectModel"
    private let postEntityName = "\(CDPost.self)"
    private let postScoreKey = "score"
    
    // MARK: - Persistent Container
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: modelName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Saving
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

// MARK: - PostDataSource

extension CoreDataManager: PostDataSource {
    
    func getAll() -> [Post] {
        let cdPosts = getAllPostModels()
        return cdPosts.compactMap { Post(coreDataModel: $0) }
    }
    
    func save(_ posts: [Post]) {
        for post in posts {
            save(post)
        }
        saveContext()
    }
    
    func save(_ post: Post) {
        let context = persistentContainer.viewContext
        let _ = CDPost(post: post, context: context)
    }
    
    func deleteAll() {
        let posts = getAllPostModels()
        let context = persistentContainer.viewContext
        
        for post in posts {
            context.delete(post)
        }
    }
    
    private func getAllPostModels() -> [CDPost] {
        let context = persistentContainer.viewContext
        let request = NSFetchRequest<CDPost>.init(entityName: postEntityName)
        request.sortDescriptors = [NSSortDescriptor(key: postScoreKey, ascending: false)]
        return (try? context.fetch(request)) ?? []
    }
    
}
