//
//  Post.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 6/4/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import Foundation

class Post {
    
    // MARK: - Properties
    
    private(set) var title: String
    private(set) var author: String?
    private(set) var createdDate: Date?
    private(set) var numberOfComments: Int = 0
    private(set) var score: Int = 0
    
    private(set) var url: URL?
    private(set) var thumbnailUrl: URL?
    
    var createdHoursAgo: Int {
        guard let date = createdDate else { return 0 }
        let interval = NSCalendar.current.dateComponents([.hour], from: date, to: Date())
        return interval.hour ?? 0
    }
    
    // MARK: - Init
    
    init?(data: [String: Any]) {
        guard let title = data[CodingKeys.title] as? String else { return nil }
        
        self.title = title
        author = data[CodingKeys.author] as? String
        numberOfComments = (data[CodingKeys.numberOfComments] as? Int) ?? 0
        score = (data[CodingKeys.score] as? Int) ?? 0
        
        if let created = data[CodingKeys.created] as? Int {
            createdDate = Date(timeIntervalSince1970: TimeInterval(created))
        }
        
        if let string = data[CodingKeys.url] as? String, let url = URL(string: string), url.isValid {
            self.url = url
        }
        
        if let string = data[CodingKeys.thumbnail] as? String, let url = URL(string: string), url.isValid {
            thumbnailUrl = url
        }
    }
    
    init?(coreDataModel: CDPost) {
        guard let modelTitle = coreDataModel.title else { return nil }
        title = modelTitle
        author = coreDataModel.author
        createdDate = coreDataModel.createdDate
        numberOfComments = Int(coreDataModel.numberOfComments)
        score = Int(coreDataModel.score)
        
        url = coreDataModel.url
        thumbnailUrl = coreDataModel.thumbnailUrl
    }
    
}

// MARK: - CodingKeys

extension Post {
    
    fileprivate enum CodingKeys {
        static let title = "title"
        static let author = "author"
        static let numberOfComments = "num_comments"
        static let score = "score"
        static let created = "created_utc"
        static let url = "url"
        static let thumbnail = "thumbnail"
    }
    
}
