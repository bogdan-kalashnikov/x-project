//
//  AppDelegate.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 5/31/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func applicationWillResignActive(_ application: UIApplication) {
        CoreDataManager.shared.saveContext()
    }

}

