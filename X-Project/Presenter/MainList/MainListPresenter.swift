//
//  MainListPresenter.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 5/31/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import Foundation

final class MainListPresenter: NSObject, MainListPresenterProtocol {
    
    private let jsonUrlString = "https://www.reddit.com/top.json"
    
    private enum Localized {
        static let by = NSLocalizedString("by:", comment: "")
        static let score = NSLocalizedString("Score:", comment: "")
        static let notAvailable = NSLocalizedString("N/A", comment: "")
        static let comments = NSLocalizedString("Comments:", comment: "")
        static let hoursAgo = NSLocalizedString("hours ago", comment: "")
    }

    // MARK: - Properties
    
    private var postDataSource: PostDataSource
    private weak var mainListView: MainListView?
    
    private var posts: [Post]
    private var isloadingData: Bool = false
    
    private var afterId: String? = UserDefaults.after {
        didSet {
            UserDefaults.after = afterId
        }
    }
    
    // MARK: - Init
    
    init(dataSource: PostDataSource) {
        postDataSource = dataSource
        posts = []
    }
    
    // MARK: - MainListPresenter
    
    func attachView(_ view: MainListView) {
        mainListView = view
        updateView()
    }
    
    func didSelectPost(at index: Int) {
        guard index < posts.count, let url = posts[index].url else { return }
        mainListView?.presentSafariView(with: url)
    }
    
    // MARK: - Update
    
    private func updateView() {
        posts = postDataSource.getAll()
        mainListView?.displayPosts(posts.map { preparePost($0) })
    }
    
    private func preparePost(_ post: Post) -> PostDisplayModel {
        return PostDisplayModel(title: post.title,
                                author: Localized.by + " " + (post.author ?? Localized.notAvailable),
                                date: "\(post.createdHoursAgo) " + Localized.hoursAgo,
                                comments: Localized.comments + " \(post.numberOfComments)",
                                score: Localized.score + " \(post.score)",
                                thumbnail: post.thumbnailUrl,
                                isSelectable: (post.url != nil))
    }
    
    // MARK: - Load
    
    func loadData(option: PostsLoadOption) {
        let isForced = (option == .all && posts.isEmpty) || (option == .reload) || (option == .next)
        guard isForced, !isloadingData, let url = prepareUrl(option) else {
            mainListView?.endUpdating()
            return
        }
        isloadingData = true
        
        let request = URLRequest(url: url)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let task = session.dataTask(with: request) { [weak self] (data, response, error) in
            guard let self = self else { return }
            defer {
                DispatchQueue.main.async {
                    self.isloadingData = false
                    self.mainListView?.endUpdating()
                }
            }
            
            guard error == nil else {
                print("Download Failed With Error")
                return
            }
            
            guard let data = data else { return }
            
            let options = JSONSerialization.ReadingOptions.mutableContainers
            guard let json = (try? JSONSerialization.jsonObject(with: data, options: options)) as? [String: Any] else { return }
            
            guard let jsonData = json[CodingKeys.data] as? [String: Any] else { return }
            guard let childrenData = jsonData[CodingKeys.children] as? [[String: Any]] else { return }
            
            let postsData = childrenData.compactMap { $0[CodingKeys.data] as? [String: Any] }
            guard !postsData.isEmpty else { return }
            
            DispatchQueue.main.async {
                let posts = postsData.compactMap { Post(data: $0) }
                if option == .reload {
                    self.postDataSource.deleteAll()
                }
                self.afterId = jsonData[CodingKeys.after] as? String
                self.postDataSource.save(posts)
                self.updateView()
            }
        }
        
        task.resume()
        mainListView?.beginUpdating()
    }
    
    private func prepareUrl(_ option: PostsLoadOption) -> URL? {
        guard option == .next, let after = afterId else { return URL(string: jsonUrlString) }
        guard var urlComponents = URLComponents(string: jsonUrlString) else { return nil }
        
        urlComponents.queryItems = [
            URLQueryItem(name: CodingKeys.after, value: after),
            URLQueryItem(name: CodingKeys.count, value: "\(posts.count)"),
        ]
        return urlComponents.url
    }
    
}

// MARK: - CodingKeys

extension MainListPresenter {
    
    fileprivate enum CodingKeys {
        static let data = "data"
        static let after = "after"
        static let count = "count"
        static let children = "children"
    }
    
}
