//
//  MainListPresenterProtocol.swift
//  X-Project
//
//  Created by Bogdan Kalashnikov on 6/3/19.
//  Copyright © 2019 Bogdan Kalashnikov. All rights reserved.
//

import Foundation

enum PostsLoadOption: Int {
    case all
    case reload
    case next
}

protocol MainListPresenterProtocol {
    
    func attachView(_ view: MainListView)
    func loadData(option: PostsLoadOption)
    
    func didSelectPost(at index: Int)
    
}
